import React, { Component } from 'react';
import PropTypes from 'prop-types'
import HttpService from 'services/Http'
import { API_ADDRESS } from 'config'
import NotificationService from 'services/Notification'
import { Label, Textarea, Input, Text, Button } from 'atoms'
import { Link } from 'react-router-dom'
import {getQueryParamByName} from 'helpers/utils'

class userOrgInviteResend extends Component {
  constructor() {
    super()
    this.state = {
      error : false
    }
  }

  getNewUserInfo() {
    let token = getQueryParamByName('token')
    HttpService
    .get(`${API_ADDRESS}/organizations/invitations/basic-infos/${token}`)
    .then(result => {
      this.setState({
        token : token,
        newUserName : result.data.name,
        newUserEmail : result.data.email,
        newOrganizationName : result.data.organization_name ,
        newUserSenderName : result.data.sender_name,
        newUserSenderEmail : result.data.sender_email,
        newUserIsProvider : result.data.is_provider_yn,
        idOrg : result.data.id_org,
        userCountryCode : result.data.user_country_code
      })
    }).catch((err) => {
      this.setState({
          error : true
        })
      })
  }

  getNewInvite() {
    let theFullSave ={
         "uudi": this.state.token
      }
    HttpService
    .post(`${API_ADDRESS}/organizations/invitations/expired`,theFullSave)
    .then(result => {
      NotificationService.show(true, 'success', 'Convite solicitado com sucesso!')
    })
  }

  componentDidMount() {
    this.getNewUserInfo()
  }

  render() {
    return (

      <React.Fragment>
      {this.state.error == false
      ? <div className="login">
          <div className="box-login row">
            <div className="col-12 p-0 out-head">
              <div className="logo-konos"></div>
              <h4 className="login-text text-purple">Convite Konos</h4>
            </div>
            <div className="col-12 row form-login pr-2">
              <div className="col-12 px-0">
                <b>Esse link expirou.</b>
                <p>Entre em contato com o usuário abaixo e solicite um novo e-mail</p>
                <div className="bg-gray-light p-3 mb-3 text-purple-dark">
                  <p className='mb-0'><b>{this.state.newUserSenderName}</b> ( {this.state.newUserSenderEmail} )</p>
                  <p className='mb-0'>da <b>{this.state.newOrganizationName}</b></p>
                </div>
              </div>
              <div className="col-6 pl-0">
                <Button
                  className="bg-purple-dark text-white text-uppercase"
                  onClick={() => {
                    this.getNewInvite()}
                  }
                >solicitar novo convite</Button>
              </div>
            </div>
          </div>
        </div>
      : <div className="login">
          <div className="text-white text-center">
            <h2>Ocorreu um erro!</h2>
            <h4>Convite inválido</h4>
          </div>
        </div>
        }
      </React.Fragment>
    )
  }
}

userOrgInviteResend.contextTypes = {
  router: PropTypes.object.isRequired
}

export default userOrgInviteResend
