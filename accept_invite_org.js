import React, { Component } from 'react';
import PropTypes from 'prop-types'
import HttpService from 'services/Http'
import { API_ADDRESS } from 'config'
import NotificationService from 'services/Notification'
import { Label, Textarea, Input, Text, Button } from 'atoms'
import { Link } from 'react-router-dom'
import Toggle from 'news/atoms/Toggle'
import SimpleTable from 'molecules/SimpleTable'
import allSpecialtiesJson from 'news/molecules/Specialties/specialties.json'
import registrationTypesJson from 'news/molecules/registrationTypes/registrations_types.json'
import RadioList from 'news/molecules/radioList'
import Radio from 'atoms/Radio'
import Select from 'molecules/Select'
import _ from 'lodash'
import { setValue, getValue, depperSet } from 'helpers/utils'
import {UF,genders,attending_genders} from 'constants'
import CellPhoneInput from 'molecules/CellPhoneInput'
import * as normalizeFunction from 'helpers/normalizeFunctions'
import {getQueryParamByName, validateCPF} from 'helpers/utils'
import Modal from 'news/molecules/Modal'

class userOrgInviteAccept extends Component {
  constructor(props) {
    super(props)
    this.state = {
      newRegistration : false,
      specialtiesForInclusion : [],
      registrationsForInclusion : [],
      termsConditions : '',
      showModal:false,
      showModalterms:false,
      idOrg : '',
      error: false,
      form : {
        error: {},
        touched: {},
        registration_type : '',
        registration_value : '',
        registration_state: ''
      },
      modal: {
        title: 'Atenção',
        hasNested : false,
        classList: "modal-lg modal-dialog-centered",
        footer: () => {
          return(
            <React.Fragment>
            <Button
               className="btn-primary text-uppercase"
               onClick={() => {
                 this.context.router.history.push(`/login`)
                }
               }
               >login
            </Button>
              <Button
                className="bg-transp text-purple-dark text-uppercase"
                onClick={this.toggleModal}>
                fechar
              </Button>
            </React.Fragment>
          )
        }
      },
      modalTerms: {
        title: 'Termos de uso',
        hasNested : false,
        classList: "modal-lg modal-dialog-centered",
        footer: () => {
          return(
            <React.Fragment>
              <Button
                className="bg-transp btn-outline-primary text-purple-dark text-uppercase"
                onClick={this.toggleModalTerms}>
                fechar
              </Button>
            </React.Fragment>
          )
        }
      },
      allSpecialties : allSpecialtiesJson,
      registrationTypes : registrationTypesJson,
      newUserName : '',
      newUserEmail : '',
      newOrganizationName : '',
      newUserSenderName : '',
      newUserDoc : '',
      newUserBirthDate : '',
      newUserSex : '',
      newUserMobileNumber : '',
      newUserPassword : '',
      newUserPasswordClone : '',
      newUserSpecialties : [],
      tempSpecialty : '',
      tempSpecialtyId : '',
      tempRegistrationRqe : '',
      tempSpecialtyUf : '',
      tempRegistrationType : '',
      tempRegistrationNumber : '',
      tempRegistrationUf : '',
      tempIsPrincipal : ''
    }
  }

  getNewUserInfo() {
    let token = getQueryParamByName('token')
    HttpService
    .get(`${API_ADDRESS}/organizations/invitations/basic-infos/${token}`)
    .then(result => {
      this.setState({
        token : token,
        newUserName : result.data.name,
        newUserEmail : result.data.email,
        newOrganizationName : result.data.organization_name ,
        newUserSenderName : result.data.sender_name,
        newUserIsProvider : result.data.is_provider_yn,
        idOrg : result.data.id_org,
        userCountryCode : result.data.user_country_code
      })
    }).catch((err) => {
      this.setState({
          error : true
        })
      })
  }

  newRegistration() {
    if(this.state.newRegistration != true){
      this.setState({
        newRegistration : true
      })
    }else{
      this.setState({
        newRegistration : false
      })
    }
  }

  handleTermsConditions() {
    if(this.state.termsConditions != true){
      this.setState({
        termsConditions : true
      })
    }else{
      this.setState({
        termsConditions : false
      })
    }
  }

  saveNewUser() {

    if(this.state.newUserName != ''
      && this.state.newUserEmail != ''
      && this.state.newUserBirthDate != ''
      && this.state.newUserSex != ''
      && this.state.newUserMobileNumber != ''
      && this.state.newUserDoc != ''
      && this.state.newUserPassword != ''
      && this.state.termsConditions == true
      && this.state.registrationsForInclusion != []){
      let registrationsForInclusion = this.state.registrationsForInclusion
      let specialtiesForInclusion = this.state.specialtiesForInclusion
      let theFullSave ={
           "uudi": this.state.token,
           "name": this.state.newUserName,
           "email": this.state.newUserEmail,
           "birth_date": normalizeFunction.normalizeDate(this.state.newUserBirthDate),
           "gender_fm": this.state.newUserSex.toUpperCase(),
           "mobile_country_ddi": "11",
           "mobile_num": this.state.newUserMobileNumber.replace(/[^0-9]{1,}/g, ''),
           "document_value": this.state.newUserDoc.replace(/[\.-]/g, ''),
           "id_doc_type": "1",
           "password": this.state.newUserPassword,
           "is_provider": this.state.newUserIsProvider,
           "registries" : registrationsForInclusion.map((item) => {
               return({
                 "id_org": this.state.idOrg,
                 "country_code": this.state.userCountryCode,
                 "state": item.registrationUf,
                 "id_type": item.registrationTypeId,
                 "id_prefix": item.registrationType,
                 "id_code": item.registrationNumber
               })
             }),
              "skills": specialtiesForInclusion.map((item) => {
                return({
                  "id_specialty_ref": item.idSpecialty,
                  "is_primary_yn": item.IsPrincipal,
                  "id_specialty": item.idSpecialty,
                  "specialty": item.specialty,
                  "registries" : registrationsForInclusion.map((item) => {
                      return({
                        "country_code": this.state.userCountryCode,
                        "state": item.registrationUf,
                        "id_type": item.registrationTypeId,
                        "id_prefix": item.registrationType,
                        "id_code": item.registrationNumber
                      })
                    }),
                })
            }),
        }
      HttpService
      .post(`${API_ADDRESS}/organizations/invitations/accept-new-user`,theFullSave)
      .then((result) =>{
        NotificationService.show(true, 'success', 'Cadastro realizado com sucesso!')
          setTimeout(() => {
            this.context.router.history.push(`/login`)
          }, 3000)
        }).catch((err) => {
          if(err.response.data.user_message.message[1].user_message.message == '[S3_ACCOUNTS.users] id_doc_type and document_value already exists'){
            this.openModal()
          }else{
            NotificationService.show(true, 'error', 'Ocorreu um erro!')
          }
      })
    }else{
      NotificationService.show(true, 'error', 'Preencha todos os campos!')
    }

  }

  handleChangeSelect = option => {
    this.setState({
      tempSpecialtyId: option.id_specialty,
      tempSpecialty: option.specialty
    })
  }

  handleChangeSelectReg = option => {
    this.setState({
      tempRegistrationTypeId : option.id_provider_id_type,
      tempRegistrationType : option.provider_id_type
    })
  }

  handleChangeSelectUf = option => {
    this.setState({
      tempSpecialtyUf: option.value
    })
  }

  handleChangeSelectUfReg = option => {
    this.setState({
      tempRegistrationUf: option.value
    })
  }

  handleChangeCheckPass = (e) => {
      this.setState({
        newUserPasswordClone : e.target.value
      })
  }

  saveStateRegistration = () => {
    if(this.state.tempRegistrationType
      && this.state.tempRegistrationNumber
      && this.state.tempRegistrationUf != ''){
      this.setState({
        registrationsForInclusion :[...this.state.registrationsForInclusion,{
          registrationType : this.state.tempRegistrationType,
          registrationTypeId : this.state.tempRegistrationTypeId,
          registrationNumber : this.state.tempRegistrationNumber,
          registrationUf : this.state.tempRegistrationUf
        }],
        tempRegistrationType : '',
        tempRegistrationNumber :'',
        tempRegistrationUf : '',
        tempRegistrationTypeId : ''
      })
    }else{
      NotificationService.show(true, 'error', 'Preencha todos os campos em Registro')
    }
  }

  openModalTerms = () => {
    this.toggleModalTerms()
  }

  toggleModalTerms = () => {
    this.setState({
      showModalTerms: !this.state.showModalTerms
    })
  }


  openModal = () => {
    this.toggleModal()
  }

  toggleModal = () => {
    this.setState({
      showModal: !this.state.showModal
    })
  }

  addToTable = (type) => {
    switch(type) {
        case 'registration':
            let alreadyHas = false
            if(this.state.registrationsForInclusion.length == 0 ){
              this.saveStateRegistration()
            }else{
              this.state.registrationsForInclusion.filter((obj) => {
                if((obj.registrationUf == this.state.tempRegistrationUf) && (obj.registrationTypeId == this.state.tempRegistrationTypeId)){
                  alreadyHas = true
                  NotificationService.show(true, 'error', 'Já existe um registro deste tipo para este estado')
                }
              })
              if(alreadyHas == false){
                this.saveStateRegistration()
              }
            }
            break
        case 'specialty':
            if(this.state.tempSpecialty && this.state.tempSpecialtyUf && this.state.tempRegistrationRqe != ''){
              let isPrincipal = 'Y'
              if(this.state.specialtiesForInclusion.length > 0){
                  isPrincipal = 'N'
              }
              this.setState({
                specialtiesForInclusion :[...this.state.specialtiesForInclusion,{
                  specialty : this.state.tempSpecialty,
                  idSpecialty : this.state.tempSpecialtyId,
                  specialtyUf : this.state.tempSpecialtyUf,
                  registrationRqe : this.state.tempRegistrationRqe,
                  IsPrincipal : isPrincipal
                }],
                tempSpecialty : '',
                tempSpecialtyId : '',
                tempSpecialtyUf : '',
                tempRegistrationRqe : '',
                tempIsPrincipal : ''
              })
            }else{
              NotificationService.show(true, 'error', 'Preencha todos os campos em Especialidades')
            }
            break
        default:
    }

  }

  deleteAddedItem = (item) => {

    let ItemforRemoval = item

    if(item.idSpecialty){
      let allItems = this.state.specialtiesForInclusion
      let positionInArray = this.state.specialtiesForInclusion.indexOf(ItemforRemoval)
      let checkForPrincipal = false

      allItems.splice(positionInArray,1)
      this.setState({
        specialtiesForInclusion : allItems
      })
      //checar se tem algum principal precisa de pelo menos um principal
      if(this.state.specialtiesForInclusion.length > 0){
        this.state.specialtiesForInclusion.map((item) =>{
          if(item.IsPrincipal == 'Y'){
              checkForPrincipal = true
          }
        })
        if(checkForPrincipal == false){
          this.state.specialtiesForInclusion[0].IsPrincipal = 'Y'
        }
      }
    }

    if(item.registrationType){
      let allItems = this.state.registrationsForInclusion
      let positionInArray = this.state.registrationsForInclusion.indexOf(ItemforRemoval)

      allItems.splice(positionInArray,1)
      this.setState({
        registrationsForInclusion : allItems
      })
    }

  }

  handleRadioPrincipal = (event,item,index) => {
    let specialtiesForInclusion = this.state.specialtiesForInclusion
    let tempArray = specialtiesForInclusion

    tempArray.map((item) =>{
      item.IsPrincipal = 'N'
    })

    this.setState({
      specialtiesForInclusion : tempArray
    })

    specialtiesForInclusion[index].IsPrincipal = 'Y'
      this.setState({
          specialtiesForInclusion
      })

  }

  handleChange = (event, path) => {
    let hasError = false
    let newState = {
      ...this.state
    }

    depperSet(newState, path, event.target ? event.target.value : event,)

    let field = event.target ? event.target.name : event;

    this.setState({
      ...newState,
      error: {
        ...this.state.error, [path]: hasError
      }
    })
  }


  handleBlur = (e) => {
    if(e.target.defaultValue != ''){
      let doc = e.target.defaultValue
      let cleanDoc = doc.replace(/[\.-]/g, '')
      if (!validateCPF(cleanDoc)) {
        NotificationService.show(true, 'error', 'CPF inválido')
        this.setState({
          newUserDoc : ''
        })
      }
    }
  }

  componentDidMount() {
    this.getNewUserInfo()
  }

  render() {

    return (
      <React.Fragment>
        <div className="login">
          <Modal
            isOpen={this.state.showModal}
            toggle={this.toggleModal}
            title={this.state.modal.title}
            classList={this.state.modal.classList}
            footer={this.state.modal.footer()}
            hasNested={this.state.modal.hasNested}
          >
            <h4>Usuário já cadastrado no sistema.</h4>
            <h6>Para entrar, efetue o login.</h6>
          </Modal>
          <Modal
            isOpen={this.state.showModalTerms}
            toggle={this.toggleModalTerms}
            title={this.state.modalTerms.title}
            classList={this.state.modalTerms.classList}
            footer={this.state.modalTerms.footer()}
            hasNested={this.state.modalTerms.hasNested}
          >
          <p>Lorem Ipsum</p>
          </Modal>
            {this.state.newRegistration == false && this.state.error == false
              ? <div className="box-login row">
                  <div className="col-12 p-0 out-head">
                    <div className="logo-konos"></div>
                    <h4 className="login-text text-purple">Convite Konos</h4>
                  </div>
                  <div className="col-12 row px-3 form-login">
                    <div className="col-12">
                      <h5>Olá <b>{this.state.newUserName}</b>, tudo bem?</h5>
                      <p>Você aceitou o convite de <b>{this.state.newUserSenderName}</b> para fazer parte do Konos na empresa <b>{this.state.newOrganizationName}</b>. O que você gostaria de fazer?</p>
                    </div>
                    <div className="col-6">
                      <Link to={'/login'}>
                        <Button
                          className="bg-purple-dark text-white text-uppercase"
                        >
                        quero efetuar o login</Button>
                      </Link>
                    </div>
                    <div className="col-6">
                      <Button
                        className="bg-purple-dark text-white text-uppercase"
                        onClick={() => {
                        this.newRegistration()}
                        }
                        >quero me cadastrar</Button>
                    </div>
                  </div>
                </div>
                : this.state.newRegistration == false && this.state.error == true
                ? <div className="text-white text-center">
                    <h2>Ocorreu um erro!</h2>
                    <h4>Convite inválido</h4>
                  </div>
                : null
              }
            {this.state.newRegistration == true
              ?  <div className={`box-login row ` + (this.state.newUserIsProvider == 'Y' ? 'lg-box' : 'lg-box-no-height')}>
                    <div className="col-12 p-0 out-head">
                      <div className="logo-konos"></div>
                      <h4 className="login-text text-purple">Efetue seu cadastro</h4>
                    </div>
                    <div className="col-12 row px-2 form-login">
                      <div className="col-12"><h6 className="font-weight-bold">Para acessar o Konos, preencha os dados abaixo.</h6></div>
                      <div className="col-12 col-md-8 pb-3">
                        <label className="lbl text-uppercase required">nome</label>
                        <Input
                        type="text"
                        name="name"
                        className='no-brd-input'
                        placeholder="Nome Completo"
                        label="nome completo"
                        onChange={(event) => this.handleChange(event,'newUserName')}
                        value={getValue(this.state, 'newUserName')}
                        required
                        isvalid='false'
                        />
                      </div>
                      <div className="col-12 col-md-4 pb-3">
                        <label className="lbl text-uppercase required">cpf</label>
                        <Input
                            name="document_value"
                            mask="111.111.111-11"
                            placeholder="Ex: 000.000.000-00"
                            className="no-brd-input"
                            onChange={(event) => this.handleChange(event,'newUserDoc')}
                            onBlur={this.handleBlur}
                            value={getValue(this.state, 'newUserDoc')}
                            required
                          />
                      </div>
                      <div className="col-12 col-md-4 pb-3">
                        <label className="lbl text-uppercase required">data de nascimento</label>
                        <Input
                          name="birth_date"
                          placeholder="Ex: 00/00/0000"
                          className='no-brd-input'
                          mask="11/11/1111"
                          onChange={(event) => this.handleChange(event,'newUserBirthDate')}
                          value={getValue(this.state, 'newUserBirthDate')}
                          required
                          />
                      </div>
                      <div className="col-12 col-md-4 pb-3">
                        <label className="lbl text-uppercase required">sexo</label>
                        <RadioList
                          name="gender_fm"
                          label="Sexo"
                          items={genders}
                          onChange={(event) => this.handleChange(event,'newUserSex')}
                          valueChecked={getValue(this.state,'newUserSex')}
                          required
                        />
                      </div>
                      <div className="col-12 col-md-4 pb-3">
                        <label className="lbl text-uppercase required">celular</label>
                        <CellPhoneInput
                          name='mobileNumber'
                          autoComplete="off"
                          placeholder="(99) 9 9999-9999"
                          className="no-brd-input"
                          onChange={(event) => this.handleChange(event,'newUserMobileNumber')}
                          value={getValue(this.state, 'newUserMobileNumber')}
                          required
                        />
                      </div>
                      <div className="col-12 col-md-8 pb-3">
                        <label className="lbl text-uppercase required">e-mail</label>
                        <Input
                          type="text"
                          name="email"
                          className='no-brd-input'
                          autoComplete="off"
                          placeholder="email@provedor.com"
                          onChange={(event) => this.handleChange(event,'newUserEmail')}
                          value={getValue(this.state, 'newUserEmail')}
                          required
                        />
                      </div>
                      <div className="col-12 col-md-4 pb-3">
                        <label className="lbl text-uppercase required">profissional da saúde</label>
                        <div className="input form-control no-brd-input disabled">
                            <Toggle
                              name='provider'
                              isChecked={this.state.newUserIsProvider == 'Y' ? true : false}
                              disabled={true}
                            />
                            {/*<i data-tip="tooltip" className="icon-duvida-peq text-gray-medium float-right "></i>*/}
                          </div>
                      </div>
                      <div className='col-12 row'>

                      {this.state.newUserIsProvider == 'Y'
                      ? <React.Fragment>
                          <div className='col-12'><h6 className="font-weight-bold pt-3">Registro</h6></div>
                          <div className='row ml-3 w-100 border py-3 mb-3'>
                            <div className='col-12 col-md-4'>
                              <label className="lbl text-uppercase required">tipo de registro</label>
                              <Select
                                placeholder="Selecione"
                                className="p-0 no_x"
                                options={this.state.registrationTypes.map((registrationTypes) => {
                                  registrationTypes.label = registrationTypes.provider_id_type
                                  registrationTypes.value = registrationTypes.id_provider_id_type
                                  return registrationTypes
                                })}
                                onChange={this.handleChangeSelectReg}
                                value={this.state.tempRegistrationTypeId}
                              />
                            </div>
                            <div className='col-12 col-md-4'>
                              <label className="lbl text-uppercase required">número do registro</label>
                              <Input
                                type="text"
                                name="registration_number"
                                className='no-brd-input'
                                placeholder='Digite o número'
                                onChange={(event) => this.handleChange(event,'tempRegistrationNumber')}
                                value={getValue(this.state, 'tempRegistrationNumber')}
                              />
                            </div>
                            <div className='col-12 col-md-4'>
                              <label className="lbl text-uppercase required">estado do registro</label>
                              <Select
                                placeholder="Selecione"
                                className="p-0 no_x"
                                options={UF}
                                onChange={this.handleChangeSelectUfReg}
                                value={this.state.tempRegistrationUf}
                              />
                            </div>
                            <div className='col-12'>
                              <Button
                                 className="btn-sm btn-outline-primary mt-3 text-uppercase"
                                 onClick={() => {this.addToTable('registration')}}
                                >adicionar
                              </Button>
                            </div>
                          </div>
                        </React.Fragment>
                      : null
                      }

                        <div className='w-100'>

                        {this.state.registrationsForInclusion.length > 0 ?
                          <React.Fragment>
                            <div className='col-12'><h6 className="font-weight-bold pt-3">Registros adicionados</h6></div>
                            <table className='table-sm shadow-head w-100 ml-3 my-3'>
                              <thead>
                                <tr>
                                  <th>TIPO DE REGISTRO</th>
                                  <th>NÚM. DO REGISTRO</th>
                                  <th>ESTADO</th>
                                  <th></th>
                                </tr>
                              </thead>
                              <tbody>
                              {this.state.registrationsForInclusion.map((item,index) => {
                                return(
                                  <React.Fragment key={`key`+index}>
                                    <tr key={item.id_exam}>
                                      <td>{item.registrationType}</td>
                                      <td>
                                        {item.registrationNumber}
                                      </td>
                                      <td>{item.registrationUf}</td>
                                      <td className='delete-examination cursor-pointer'>
                                        <span onClick={() => {this.deleteAddedItem(item)}}>
                                          <i className="icon-lixeira-peq"></i>
                                        </span>
                                      </td>
                                    </tr>
                                  </React.Fragment>
                                  )
                                })
                              }
                              </tbody>
                            </table>
                          </React.Fragment>
                          :null
                        }

                        </div>
                        {this.state.newUserIsProvider == 'Y'
                        ? <React.Fragment>
                          <div className='col-12'><h6 className="font-weight-bold pt-3">Especialidade</h6></div>
                          <div className='row ml-3 w-100 border py-3 mb-3'>
                            <div className='col-12 col-md-4'>
                              <label className="lbl text-uppercase">especialidade</label>
                              <Select
                                placeholder="Selecione"
                                className="p-0 no_x"
                                options={this.state.allSpecialties.map((allSpecialties) => {
                                  allSpecialties.label = _.capitalize(allSpecialties.specialty)
                                  allSpecialties.value = allSpecialties.id_specialty
                                  return allSpecialties
                                })}
                                onChange={this.handleChangeSelect}
                                value={this.state.tempSpecialtyId}
                              />
                            </div>
                            <div className='col-12 col-md-4'>
                              <label className="lbl text-uppercase">número do registro rqe</label>
                              <Input
                                type="text"
                                name="registration_number_rqe"
                                className='no-brd-input'
                                placeholder='Digite o número'
                                onChange={(event) => this.handleChange(event,'tempRegistrationRqe')}
                                value={getValue(this.state, 'tempRegistrationRqe')}
                              />
                            </div>
                            <div className='col-12 col-md-4'>
                              <label className="lbl text-uppercase">estado do registro rqe</label>
                              <Select
                                placeholder="Selecione"
                                className="p-0 no_x"
                                options={UF}
                                onChange={this.handleChangeSelectUf}
                                value={this.state.tempSpecialtyUf}
                              />
                            </div>

                            <div className='col-12'>
                              <Button
                                 className="btn-sm btn-outline-primary mt-3 text-uppercase"
                                 onClick={() => {this.addToTable('specialty')}}
                                >adicionar
                              </Button>
                            </div>
                          </div>
                          </React.Fragment>
                          : null}
                      </div>

                      <div className='w-100'>
                      {this.state.specialtiesForInclusion.length > 0 ?
                        <React.Fragment>
                          <div className='col-12'>
                            <h6 className="font-weight-bold pt-3">Especialidades adicionadas</h6>
                            <table className='table-sm shadow-head w-100 my-3'>
                              <thead>
                                <tr>
                                  <th>PRINCIPAL</th>
                                  <th>ESPECIALIDADE</th>
                                  <th>REGISTRO RQE</th>
                                  <th> </th>
                                </tr>
                              </thead>
                              <tbody>
                              {this.state.specialtiesForInclusion.map((item,index) => {
                                return(
                                  <React.Fragment key={`key`+index}>
                                    <tr>
                                      <td>
                                        <Button
                                          className={item.IsPrincipal == 'Y' ? 'bg-transp btn-sm text-purple-dark' : 'bg-transp btn-sm text-gray'}
                                          id={'item-'+index}
                                          onClick={(event) => this.handleRadioPrincipal(event,item,index)}
                                          >
                                          <i className="fa fa-check-circle"></i>
                                        </Button>
                                      </td>
                                      <td>
                                        {item.specialty}
                                      </td>
                                      <td>{item.specialtyUf} / {item.registrationRqe}</td>
                                      <td className='delete-examination cursor-pointer'>
                                        <span onClick={() => {this.deleteAddedItem(item)}}>
                                          <i className="icon-lixeira-peq"></i>
                                        </span>
                                      </td>
                                    </tr>
                                  </React.Fragment>
                                  )
                                })
                              }
                              </tbody>
                            </table>
                          </div>
                        </React.Fragment>
                        :null
                      }
                      </div>

                      <div className="col-12 col-md-6">
                        <label className="lbl text-uppercase required">senha</label>
                        <Input
                          name="user_pass"
                          type="password"
                          placeholder="Digite uma senha"
                          className='no-brd-input'
                          onChange={(event) => this.handleChange(event,'newUserPassword')}
                          required
                          />
                      </div>
                      <div className="col-12 col-md-6">
                        <label className="lbl text-uppercase required">confirmar a senha</label>
                        <Input
                          name="user_pass_clone"
                          type="password"
                          placeholder="Confirme sua senha"
                          onChange={(event) => this.handleChangeCheckPass(event)}
                          className={this.state.newUserPassword == '' ? ' disabled no-brd-input' : 'no-brd-input'}
                          required
                          />
                      </div>
                      <div className="col-12 text-right">
                      {
                        (this.state.newUserPassword == '')
                        ? null
                        : (this.state.newUserPassword == this.state.newUserPasswordClone)
                        ? <small className="text-green">senhas iguais</small>
                        : <small className="text-red">senhas devem ser iguais</small>
                      }
                      </div>
                      <div className="col-12 pt-3 pb-4 pl-0">
                        <label className="pl-3 pr-0 mb-0 col-1 float-left">
                          <Radio
                            type="checkbox"
                            name="conditions"
                            onChange={(event) => this.handleTermsConditions()}
                          />
                        </label>
                        <small className="pt-1 float-left">LI E CONCORDO COM OS
                          <span
                            className="text-purple-dark cursor-pointer"
                            onClick={() => {
                              this.openModalTerms()}
                            }
                            > TERMOS DE USO</span>
                        </small>
                      </div>
                      <div className="row col-12 d-flex align-items-center">
                        <div className="col-6">
                          <Button
                            className="bg-purple-dark text-white text-uppercase"
                            onClick={() => {
                              this.saveNewUser()}
                            }
                            >cadastrar</Button>
                        </div>
                        <div className="col-6 text-right align-self-center pr-2">
                          <a
                            className="text-purple-dark cursor-pointer"
                            onClick={() => {
                              this.newRegistration()}
                            }
                            >Voltar</a>
                        </div>
                      </div>
                    </div>

                </div>
              : null
            }
        </div>

      </React.Fragment>
    )
  }
}

userOrgInviteAccept.contextTypes = {
  router: PropTypes.object.isRequired
}

export default userOrgInviteAccept
