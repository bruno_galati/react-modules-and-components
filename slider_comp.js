import React, { Component } from 'react'
import HttpService from 'services/Http'
import { API_ADDRESS } from 'config'
import Collapse from 'news/atoms/Collapse'
import moment from 'moment'
import './Slider.scss'

class Slider extends Component {
  constructor(props){
    super(props)
    this.state = {
      data: []
    }
  }

  splitChunks = (arr) => {
      let chunks = [],
          i = 0,
          n = arr.length;
      while (i < n) {
        chunks.push(arr.slice(i, i += 3));
      }
      return chunks
  }


  render() {
    let arrayNoEmptyValues = []
    this.props.value.map((item) => {
      //remove itens com id_specialty = null
      if(item.id_specialty != null){
        arrayNoEmptyValues.push(item)
      }
    })
    let chunks = this.splitChunks(arrayNoEmptyValues)
    return(
      <div id="carousel"
        className={`carousel slide ` + this.props.sliderType}
        data-interval="false"
        data-wrap="false"
        >
        <span className="optional_message">{this.props.sliderMessage}</span>
        <div className="carousel-inner">
          {chunks.map((item, indice) =>  {
              return (
                  <div
                    key={`key`+indice}
                    className={`carousel-item row no-gutters ` + (indice == 0 ? ' active '  : '')} >
                  {item.map((itemCarrousel) => {
                    return(
                      <div
                        key={`key`+itemCarrousel._id}
                        className={`col-4 text-uppercase item_carrousel item_history_`+itemCarrousel._id}
                        onClick={this.props.onClick}
                        >
                        <ul
                          className={this.props.activeItemSlider == `item_history_`+itemCarrousel._id ? `bg-gray text-purple-dark p-2 text-ellipsis` : `p-2 text-ellipsis`}
                          datasliderid={itemCarrousel._id}
                          >
                          <li>{itemCarrousel.service}</li>
                          <li>{itemCarrousel.provider.provider_name}</li>
                          <li>{moment(itemCarrousel.dt_date, 'YYYY-MM-DD').format('DD/MM/YYYY')}</li>
                        </ul>
                      </div>
                      )
                    }
                  )}
                  </div>
              )})
            }


        </div>
        <a className="carousel-control-prev" href="#carousel" role="button" data-slide="prev">
          <i className="icon-seta-line rotate-270"></i>
        </a>
        <a className="carousel-control-next" href="#carousel" role="button" data-slide="next">
          <i className="icon-seta-line rotate-90"></i>
        </a>

      </div>
    )
  }
}
Slider.defaultProps = {
  value:[],
  sliderType: '',
  sliderMessage : ''
}
export default Slider
