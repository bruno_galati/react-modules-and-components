import React, { Component } from 'react';
import PropTypes from 'prop-types'
import HttpService from 'services/Http'
import { API_ADDRESS } from 'config'
import NotificationService from 'services/Notification'
import { Label, Textarea, Input, Text, Button } from 'atoms'
import { Link } from 'react-router-dom'
import {getQueryParamByName} from 'helpers/utils'

class userOrgInviteRefuse extends Component {
  constructor(props) {
    super(props)
    this.state = {
      newUserName: '',
      newOrganizationName: '',
      newUserSenderName: '',
      error : false
    }
  }

  componentDidMount() {
    this.getNewUserInfo()
  }

  getNewUserInfo() {
    let token = getQueryParamByName('token')
    HttpService
    .get(`${API_ADDRESS}/organizations/invitations/basic-infos/${token}`)
    .then(result => {
      this.setState({
        token : token,
        newUserName : result.data.name,
        newOrganizationName : result.data.organization_name ,
        newUserSenderName : result.data.sender_name
      })
      this.refuseInvite()
    }).catch((err) => {
      this.setState({
          error : true
        })
      })
  }

  refuseInvite() {
    let refuseData ={
        'uudi': this.state.token
      }
    HttpService
    .post(`${API_ADDRESS}/organizations/invitations/refuse`,refuseData)
    .then((result) =>{
        NotificationService.show(true, 'success', 'Seu convite foi cancelado!')
        }
      )
  }

  render() {
    return (

      <React.Fragment>
        <div className="login">
        {this.state.error == false
          ? <div className="box-login row">
              <div className="col-12 p-0 out-head">
                <div className="logo-konos"></div>
                <h4 className="login-text text-purple">Convite Konos</h4>
              </div>
              <div className="col-12 row form-login">
                  <div className="col-12">
                    <h5>Olá <b>{this.state.newUserName}</b>, tudo bem?</h5>
                    <p>Você recusou o convite de <b>{this.state.newUserSenderName}</b> para fazer parte do Konos na empresa <b>{this.state.newOrganizationName}</b>.</p>
                  </div>
                  <div className="col-6">
                    <Link to={'/login'}>
                      <Button
                        className="bg-purple-dark text-white text-uppercase"
                      >
                      ir para o login</Button>
                    </Link>
                  </div>
                </div>
             </div>
          :  <div className="text-white text-center">
              <h2>Ocorreu um erro!</h2>
              <h4>Convite inválido</h4>
            </div>
        }


       </div>

      </React.Fragment>
    )
  }
}

userOrgInviteRefuse.contextTypes = {
  router: PropTypes.object.isRequired
}

export default userOrgInviteRefuse
