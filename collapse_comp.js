import React, { Component } from 'react'
import './Collapse.scss'

class Collapse extends Component {
  render() {
    return(
      <React.Fragment>
        <a
          className={`collapse_trigger ` + (this.props.className ? this.props.className : '')}
          data-toggle="collapse"
          href={`#` + this.props.target}
          role="button"
          aria-expanded={this.props.expanded}
          aria-controls={this.props.target}
          name={this.props.name ? this.props.name : this.props.target}
          target={this.props.target ? this.props.target : ''}
          >
          {this.props.title}
          <i className="float-right icon-seta-line"></i>
        </a>
        <div className="collapse w-100 show" id={this.props.target}>
            {this.props.children}
        </div>
      </React.Fragment>
    )
  }
}

export default Collapse
